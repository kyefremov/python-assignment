Сервис API по тестовому заданию
-------------------------------

Описание
========

Сервис написан на Python 3.6 (активно используются строки f'{name}')
С использованием docker, postgresql 9.6, redis, django_rq.

Fixtures для принтеров находятся в checks/fixtures.

Для отладки содержит файл populate.py, который загружает за раз три случайных сгенерированных заказа, но перед
этим должны быть загружены fixtures и добавлено csrf_exempt.

Чтобы добавить чеки с помощью CURL, нужно использовать один из api_key в принтерах,
например p5mmrd64. Также в JSON заказа ID заказа должен быть в текстовом формате, то есть в скобках,
то есть 123456 - неправильно, "123456" - правильно. Этот вариант выбран потому что
номер заказа можно сделать с буквами, например, "A123456" или "123456TEST"

Если передать в виде числа, то Django не сможет найти заказ.

Установка
=========

1) В первом терминале запустить докер

`sudo docker-compose up`

2) Во втором терминале загрузить миграции и фикстуры

`sudo docker-compose exec web python manage.py makemigrations checks`

`sudo docker-compose exec web python manage.py migrate checks`

`sudo docker-compose exec web python manage.py loaddata printer_testdata.json`

2) Во втором терминале проверить тесты

`sudo docker-compose exec web python manage.py test`

3) Во втором терминале проверить populate.py

`python3 populate.py`

Ссылки:
=======

- /checks/create_checks - создаёт чек
- /checks/check - возвращает чек в хтмл или пдф.
- /checks/list - админка чеков, без авторизации (нужно было добавлять?)
- /checks/new_checks - список новых чеков для принтера.



Тесты:
======

Добавлены тесты, проверяется всё кроме админки. В том числе создание пдф и
изменения статусов. Админка очень простая, делать для неё тесты по-моему перебор.

Известные проблемы:
===================

При остановке docker-compose в докере иногда остаётся висеть воркер, который не
даёт сразу же запустить сервис обратно. Надо подождать. Похоже это баг rq.

В settings.py внизу нужно указывать url где работает сервис. Если это возвращать
средствами django, то так как он находится в докере, то возвращается имя контейнера.

Модификации задания
===================

Внёс пару модификаций в задание, так как там вероятно были недочёты:

1) В модели Check поменял поле FileField на CharField. Так как какие-то ошибки
были с FileField по моей неопытности и в итоге так и не понял в чём его польза тут.

2) В задании написано, что 2 и 3 методы api делают GET запросы, но в примере
делается POST запрос. Оставил GET запрос.

3) Третий метод в задании передаёт ключ api и тип чека, но очевидно, что тип
чека передавать не обязательно, так как у каждого принтера свой тип сам по себе.

4) Второй метод в задании возвращает id, не понял куда должны эти id указывать.
Поэтому убрал их вообще.
