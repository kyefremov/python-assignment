from random import shuffle, randint, choice
import json
import requests

def generate_order():
    items = [
        ["Филадельфия Лайт", 199],
        ["Запеченное сердце самурая", 269],
        ["Запеченная Филадельфия", 329],
        ["Маргарита", 290],
        ["Говядина чили", 390],
        ["Джульетта", 440],
        ["Овощи в темпуре", 190],
        ["Ассорти из шашлыков", 690],
        ["Удон с курицей в остро-сладком соусе", 210]
    ]
    shuffle(items)
    items = items[0:randint(2, 5)],

    new_items = []
    price = 0
    for item in items[0]:
        new_items.append(
            { 'name': item[0], 'unit_price': item[1], 'quantity': 1 }
        )
        price += item[1]

    return {
        'order': {
            'id': f"{randint(0, 100)}",
            'price': price,
            'items': new_items,
            'address': choice(["Уфа", "Стерлитамак", "Ишимбай", "Салават"]),
            'client': {
                'name': choice(["Андрей", "Иван", "Марина", "Геннадий"]),
                'phone': choice(["917", "347", "937", "960"]) + str(randint(100000, 999999))
            },
            'point_id': randint(1, 2),
        }
    }


for i in range(3):
    r = requests.post(
        'http://localhost:8000/checks/create_checks',
        json=generate_order()
    )
