from django.db import models
from django.contrib.postgres.fields import JSONField

class Printer(models.Model):
    name = models.CharField(max_length=20)
    api_key = models.CharField(max_length=8, unique=True)
    check_type = models.CharField(
        max_length=10,
        choices=(
            ('kitchen', 'kitchen'),
            ('client', 'client'),
        )
    )
    point_id = models.IntegerField()


class Check(models.Model):
    printer_id = models.ForeignKey('Printer')
    type = models.CharField(
        max_length=10,
        choices=(
            ('kitchen', 'kitchen'),
            ('client', 'client'),
        )
    )
    order = JSONField()
    status = models.CharField(
        max_length=10,
        choices=(
            ('new','new'),
            ('rendered','rendered'),
            ('printed','printed'),
        ),
        default='new'
    )
    pdf_file = models.CharField(
        max_length=100,
    )
