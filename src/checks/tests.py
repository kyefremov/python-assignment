import json
import os
from urllib.parse import urlencode
from django.test import TestCase, Client
from django_rq import get_worker
from django.conf import settings
from checks.models import Check, Printer

class ChecksAppTest:

    fixtures = ['printer_testdata.json']
    client = Client()


    def setUp(self):
        self.order = {
            'order': {
                'id': "123456TEST",
                'price': "780",
                'items': [
                    {
                        'name': 'Вкусная пицца',
                        'quantity': 2,
                        'unit_price': 250
                    },
                    {
                        'name': 'Не менее вкусный ролл',
                        'quantity': 1,
                        'unit_price': 280
                    },
                ],
                'address': 'г. Уфа, ул. Ленина, д. 42',
                'client': {
                    'name': 'Иван',
                    'phone': '9173332222'
                },
                'point_id': 1, # указывает на принтер 1й точки
            }                  # которая должна быть в fixtures
        }
        self.order_json = json.dumps(self.order)
        self.printer = Printer.objects.all()[0]


    def tearDown(self):
        """ удаляем тестовые пдф """
        path = settings.PDF_FOLDER + "/" + self.order['order']['id']
        for file in [f"{path}_client.pdf", f"{path}_kitchen.pdf"]:
            if os.path.isfile(file):
                os.remove(file)


    def post_test_order(self, order=None):
        """ Постит тестовый заказ """
        order = self.order_json if order is None else json.dumps(order)
        response = self.client.post(
            '/checks/create_checks',
            order,
            content_type='application/json',
        )
        response.content = response.content.decode('UTF-8')
        return response



class CreateChecksTest(ChecksAppTest, TestCase):

    def test_create_ok(self):
        """ Проверяет постятся ли чеки """
        response = self.post_test_order()
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            response.content,
            {'ok': 'Чеки успешно созданы' })

        get_worker().work(burst=True)

        checks = Check.objects.filter(order__id=self.order['order']['id'])
        for check in checks:
            self.assertEqual(check.status, 'rendered')


    def test_already_created(self):
        """ Проверяет если уже созданы чеки для заказа """

        # постим два раза тестовый заказ
        self.post_test_order()
        response = self.post_test_order()

        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            response.content,
            {'error': 'Для данного заказа уже созданы чеки' })


    def test_no_printer(self):
        """ Проверяет если нет принтера для точки """
        order = self.order
        order['order']['point_id'] = 1000
        response = self.post_test_order(order)
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            response.content,
            {'error': 'Для данной точки не настроены принтеры' })


class CheckTest(ChecksAppTest, TestCase):

    def test_unauthorized(self):
        params = {'api_key': '1'}
        response = self.client.get('/checks/check', params)
        self.assertEqual(response.status_code, 401)
        self.assertJSONEqual(
            response.content.decode('UTF-8'),
            {'error': 'Ошибка авторизации'}
        )


    def test_no_check_for_order(self):
        params = {
            'api_key': self.printer.api_key,
            'format': 'html',
            'order_id': 100000,
        }
        response = self.client.get('/checks/check', params)
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(
            response.content.decode('UTF-8'),
            {'error': 'Для данного заказа нет чеков'})


    def test_create_html(self):
        """ Проверяет, что отдаётся html с чеком """
        order_id = self.order['order']['id']
        check_type = self.printer.check_type

        self.post_test_order()
        self.assertEqual(
            len(Check.objects.filter(order__id=order_id)),
            2
        )
        params = {
            'api_key': self.printer.api_key,
            'format': 'html',
            'order_id': order_id,
        }
        response = self.client.get('/checks/check', params)

        self.assertEqual(response.status_code, 200)

        # проверяем только названия позиций, предполагая, что если они есть
        # в файле, то есть и всё остальное
        # остальное можно добавить потом, если нужно будет
        for item in self.order['order']['items']:
            self.assertIn(item['name'], response.content.decode('UTF-8'))

        # запрос html не должен переводить чек в printed
        check = Check.objects.filter(order__id=order_id, type=check_type)[0]
        self.assertNotEqual(check.status, 'printed')


    def test_create_pdf(self):
        """ Проверяет, что отдаётся pdf с чеком и меняется статус на printed """

        order_id = self.order['order']['id']
        check_type = self.printer.check_type
        api_key = self.printer.api_key

        self.post_test_order()

        params = {
            'api_key': api_key,
            'format': 'pdf',
            'order_id': order_id,
        }
        pdf_api = self.client.get('/checks/check', params)

        get_worker().work(burst=True)

        # сравним ответ с файлом в папке media/pdf
        filepath = f"{settings.PDF_FOLDER}/{order_id}_{check_type}.pdf"
        with open(filepath, 'rb') as f:
            pdf_local = f.read()

        self.assertEqual(pdf_api.getvalue(), pdf_local)   # getvalue - важно!

        # запрос pdf переводит чек в статус printed
        check = Check.objects.filter(order__id=order_id, type=check_type)[0]
        self.assertEqual(check.status, 'printed')


class NewChecksTest(ChecksAppTest, TestCase):
    """ Тестирует отдачу новых чеков """

    def test_unauthorized(self):
        params = {'api_key': '1'}
        response = self.client.get('/checks/new_checks', params)
        self.assertEqual(response.status_code, 401)
        self.assertJSONEqual(
            response.content.decode('UTF-8'),
            {'error': 'Ошибка авторизации'}
        )


    def test_new_checks_ok(self):
        """ Проверяет, что возвращается список новых чеков """

        self.post_test_order()

        order_id = self.order['order']['id']
        api_key = self.printer.api_key
        check_type = self.printer.check_type

        response = self.client.get(
            '/checks/new_checks',
            { 'api_key': api_key },
        )

        params = {
            'order_id': order_id,
            'type': check_type,
            'format': 'pdf',
            'api_key': api_key,
        }
        url = f"http://testserver/checks/check?{urlencode(params)}"
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            response.content.decode('UTF-8'),
            {'checks': [url]}
        )


    def test_new_checks_empty(self):
        """ Проверяет, что возвращается пустой список новых чеков """

        order_id = self.order['order']['id']
        api_key = self.printer.api_key
        check_type = self.printer.check_type

        response = self.client.get(
            '/checks/new_checks',
            { 'api_key': api_key },
        )

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            response.content.decode('UTF-8'),
            {'checks': []}
        )
