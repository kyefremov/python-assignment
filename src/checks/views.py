import json
import requests
from base64 import b64encode
from urllib.parse import urlencode
from django.http import HttpResponse, JsonResponse, FileResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt  # чтобы загрузить данные скриптом
import django_rq

from checks.models import Printer, Check


@csrf_exempt
def create_checks(request):
    """ Создаёт чеки из JSON запроса """

    # нужны ли дополнительные проверки?

    order = json.loads(request.body)['order']

    # проверяем если заказ уже создан
    if Check.objects.filter(order__id=order['id']):
        return JsonResponse(
            {'error': 'Для данного заказа уже созданы чеки'},
            status=400)

    # загружаем принтеры для точки
    printers = Printer.objects.filter(point_id=order['point_id'])
    if len(printers) != 2:
        return JsonResponse(
            {'error': 'Для данной точки не настроены принтеры'},
            status=400)

    # создаём чеки
    for printer in printers:
        c = Check()
        c.printer_id = printer
        c.type = printer.check_type
        c.order = order
        c.status = 'new'
        c.pdf_file = ''
        c.save()

        django_rq.enqueue(
            create_pdf,
            order=order,
            check_type=printer.check_type,

        )

    return JsonResponse(
        {'ok': 'Чеки успешно созданы'},
        status=200)


def create_pdf(order, check_type):
    """ Создаёт pdf файл, обращаясь к сервису wkhtmltopdf """

    html = render_to_string(f'checks/{check_type}.html', order)

    base64_bytes = b64encode(html.encode())
    base64_string = base64_bytes.decode('utf-8')

    data = {
        'contents': base64_string,
    }

    response = requests.post(
        settings.HTML_TO_PDF_URL,
        data=json.dumps(data),
        headers={'Content-Type': 'application/json'}
    )

    filename = f"{settings.PDF_FOLDER}/{order['id']}_{check_type}.pdf"
    with open(filename, 'wb') as f:
         f.write(response.content)

    check = Check.objects.filter(order__id=order['id'], type=check_type)
    if check:
        check = check[0]
        check.status = 'rendered'
        check.pdf_file = filename
        check.save()


def check(request):
    """ Отдаёт чек в html/pdf формате при запросе api_key

    html чеки отдаются при любом статусе и правильном api_key.
    pdf чеки отдаются при статусе rendered или printed, в противном случае
    отдаётся ошибка.
    """
    api_key = request.GET.get('api_key')
    order_id = request.GET.get('order_id')
    format = request.GET.get('format')

    p = Printer.objects.filter(api_key=api_key)
    if not p:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)
    p = p[0]

    c = Check.objects.filter(order__id=order_id, type=p.check_type)
    if not c:
        return JsonResponse(
            {'error': 'Для данного заказа нет чеков'},
            status=400
        )
    c = c[0]

    if format == 'html':
        template_path = f'checks/{p.check_type}.html'
        return render(request, template_path, c.order)
    elif format == 'pdf':
        if c.status in ['rendered', 'printed']:
            return_pdf = FileResponse(
                open(c.pdf_file, 'rb'),
                content_type='application/pdf'
            )
            c.status = 'printed'
            c.save()
            return return_pdf
        else:
            return JsonResponse(
                {'error': 'Для данного заказа не сгенерирован чек в формате PDF'},
                status=400
            )
    else:
        return JsonResponse({'error': 'bad request'}, status=400)


def new_checks(request):
    """ Проверяет наличие новых чеков """
    api_key = request.GET.get('api_key')
    p = Printer.objects.filter(api_key=api_key)
    if not p:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)
    p = p[0]

    new_checks = Check.objects.filter(printer_id=p, status='rendered')
    return_list = { 'checks': [] }

    for new_check in new_checks:
        params = {
            'order_id': new_check.order['id'],
            'type': new_check.type,
            'format': 'pdf',
            'api_key': api_key,
        }
        url = settings.SERVICE_URL + '/checks/check'
        return_list['checks'].append(url + '?' + urlencode(params))

    return JsonResponse(return_list, status=200)


def list_checks(request):
    """ Выводит список чеков с возможность сортировки """
    check_type = request.GET.get('check_type')
    status = request.GET.get('status')
    api_key = request.GET.get('api_key')

    checks = Check.objects.all()
    if check_type:
        checks = checks.filter(type=check_type)
    if status:
        checks = checks.filter(status=status)
    if api_key:
        printer = Printer.objects.filter(api_key=api_key)[0]
        checks = checks.filter(printer_id=printer)

    context = {
        'printers': Printer.objects.all(),
        'statuses': ['new', 'rendered', 'printed'],
        'check_types': ['kitchen', 'client'],
        'selected': {
            'check_type': check_type,
            'status': status,
            'api_key': api_key
        },
        'checks': checks
    }

    return render(request, 'checks/list.html', context)
