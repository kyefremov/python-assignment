FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
RUN mkdir /src
ADD config/requirements.txt /config/
RUN pip install -r /config/requirements.txt
WORKDIR /src
CMD gunicorn assignment.wsgi -b 0.0.0.0:8000 & python manage.py rqworker
